# -*- coding: UTF-8 -*-
"""
Author: Jaime Rivera
File: quickmatte_executable.py
Date: 2019.01.04
Revision: 2019.01.04
Copyright: Copyright 2019 Jaime Rivera

           Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
           documentation files (the "Software"), to deal in the Software without restriction, including without
           limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
           the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
           conditions:

           The above copyright notice and this permission notice shall be included in all copies or substantial
           portions of the Software.

           THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
           TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
           SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
           ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
           OR OTHER DEALINGS IN THE SOFTWARE.

Brief: Executable file to create a quick card-based matte painting setup in Nuke

"""

__author__ = 'Jaime Rivera <www.jaimervq.com>'
__copyright__ = 'Copyright 2019, Jaime Rivera'
__credits__ = []
__license__ = 'MIT License'
__maintainer__ = 'Jaime Rivera'
__email__ = 'jaime.rvq@gmail.com'
__status__ = 'Testing'


import nuke


# -------------------------------- INPUT -------------------------------- #
cardnumber = None

while cardnumber is None:
    try:
        cardnumber = abs(int(nuke.getInput("Please indicate number of cards")))
    except ValueError:
        nuke.message("Not a valid number")


# -------------------------------- NODE CREATION -------------------------------- #

#Creation of the scene
mattescn = nuke.nodes.Scene( name="Scene_MATTE",
                             xpos=10,
                             ypos=150 )

mattescn.setSelected(True)


for i in range(0,cardnumber):

    #Creation of cards
    mattecard = nuke.nodes.Card2( name = "Card_Matte_{}".format(i+1) ,
                                  label = "Dist: [value z]",
                                  xpos = 150*i,
                                  ypos = 0,
                                  z = 5*(1+i))

    checker = nuke.nodes.CheckerBoard2( name= "Checker_matte " +str(i+1),
                                        centerlinewidth=0,
                                        xpos = 150 * i,
                                        ypos = -100 )

    mattecard.setInput(0,checker)

    mattecard['lens_in_focal'].setExpression("Camera_MATTE.focal")
    mattecard['lens_in_haperture'].setExpression("Camera_MATTE.haperture")

    mattescn.setInput(i, mattecard)
    mattecard.setSelected(True)


#Creation of the Camera
mattecam = nuke.nodes.Camera2( name="Camera_MATTE",
                               label="Focal lenght: [value focal]\nHoriz. aperture: [value haperture]",
                               xpos=350,
                               ypos=279 )

mattecam.setSelected(True)


#Creation of the scanline render
matteslr = nuke.nodes.ScanlineRender( name="ScanlineRender_MATTE",
                                      xpos=0,
                                      ypos=300 )

matteslr.setInput(1,mattescn)
matteslr.setInput(2,mattecam)
matteslr.setSelected(True)


#Creation of the ZDefocus
mattezdef = nuke.nodes.ZDefocus2( name="ZDefocus_MATTE",
                                  xpos=0,
                                  ypos=450,
                                  disable= True )

mattezdef.setInput(0,matteslr)


# -------------------------------- FUNCTION FOR EXTRA CARDS -------------------------------- #

def extracard():
    cardnames = []
    for card in nuke.allNodes("Card2"):
        if "Card_Matte_" in card.name():
            cardnames.append(int(card.name()[11:]))

    currentcardmax = max(cardnames)
    currentcardmax  += 1

    mattecard = nuke.nodes.Card2( name = "Card_Matte_{}".format(currentcardmax) ,
                                  label = "Dist: [value z]",
                                  xpos = 150*(currentcardmax-1),
                                  ypos = 0,
                                  z = 5*(currentcardmax) )

    checker = nuke.nodes.CheckerBoard2( name= "Checker_matte " +str(currentcardmax),
                                        centerlinewidth=0,
                                        xpos = 150*(currentcardmax-1),
                                        ypos = -100 )

    mattecard.setInput(0,checker)

    mattecard['lens_in_focal'].setExpression("Camera_MATTE.focal")
    mattecard['lens_in_haperture'].setExpression("Camera_MATTE.haperture")

    mattescn.setInput(currentcardmax, mattecard)
    mattecard.setSelected(True)
    checker.setSelected(True)

    nuke.zoomToFitSelected()
    for n in nuke.allNodes():
        n.setSelected(False)


# -------------------------------- FRAMING -------------------------------- #

#Framing the nodes
nuke.zoomToFitSelected()
for n in nuke.allNodes():
    n.setSelected(False)

#Message with info on how to create new cards
nuke.message("For creating additional cards, call function:\n<font size='5'>extracard()")